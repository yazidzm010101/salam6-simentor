-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: May 14, 2020 at 05:20 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `simentor_devel`
--

-- --------------------------------------------------------

--
-- Table structure for table `kelas`
--

CREATE TABLE `kelas` (
  `idKelas` char(6) NOT NULL,
  `namaKelas` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kelas`
--

INSERT INTO `kelas` (`idKelas`, `namaKelas`) VALUES
('100101', 'X IPA 1'),
('100102', 'X IPA 2'),
('100201', 'X IPS 1'),
('110101', 'XI IPA 1'),
('110102', 'XI IPA 2'),
('110201', 'XI IPS 1'),
('120101', 'XII IPA 1'),
('120102', 'XII IPA 2'),
('120201', 'XII IPS 1');

-- --------------------------------------------------------

--
-- Table structure for table `pementor`
--

CREATE TABLE `pementor` (
  `idPementor` int(11) NOT NULL,
  `namaPementor` varchar(64) NOT NULL,
  `jeniskPementor` char(1) NOT NULL,
  `usernamePementor` varchar(16) NOT NULL,
  `passwordPementor` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pementor`
--

INSERT INTO `pementor` (`idPementor`, `namaPementor`, `jeniskPementor`, `usernamePementor`, `passwordPementor`) VALUES
(1, 'MUHAMMAD RIZQI PANGESTU', 'L', 'mrizqip123', '');

-- --------------------------------------------------------

--
-- Table structure for table `pementor_kehadiran`
--

CREATE TABLE `pementor_kehadiran` (
  `tanggalKehadiran` date NOT NULL,
  `idPementor` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pementor_kehadiran`
--

INSERT INTO `pementor_kehadiran` (`tanggalKehadiran`, `idPementor`) VALUES
('2020-05-14', 1);

-- --------------------------------------------------------

--
-- Table structure for table `pementor_tanggungjawab`
--

CREATE TABLE `pementor_tanggungjawab` (
  `idPementor` int(11) NOT NULL,
  `idKelas` char(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `siswa`
--

CREATE TABLE `siswa` (
  `idSiswa` int(11) NOT NULL,
  `namaSiswa` varchar(64) NOT NULL,
  `idKelas` char(6) NOT NULL,
  `jeniskSiswa` char(1) NOT NULL DEFAULT 'L'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `siswa`
--

INSERT INTO `siswa` (`idSiswa`, `namaSiswa`, `idKelas`, `jeniskSiswa`) VALUES
(1, 'RAJAA EL-MANNAN', '100101', 'L'),
(2, 'JUBAIR EL-KALEEL', '100101', 'L'),
(3, 'AMEER AL-SADRI', '100101', 'L'),
(4, 'HILMI EL-SAIDI', '100101', 'L'),
(5, 'LUBNA AL-BADOUR', '100101', 'P'),
(6, 'NAJLAA EL-JAFARI', '100101', 'P'),
(7, 'ASMAA EL-DAJANI', '100101', 'P'),
(8, 'RAHEEMA EL-RASHED', '100101', 'P');

-- --------------------------------------------------------

--
-- Table structure for table `siswa_kehadiran`
--

CREATE TABLE `siswa_kehadiran` (
  `tanggalKehadiran` date NOT NULL,
  `idSiswa` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `siswa_kehadiran`
--

INSERT INTO `siswa_kehadiran` (`tanggalKehadiran`, `idSiswa`) VALUES
('2020-05-14', 2),
('2020-05-14', 3),
('2020-05-14', 4);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `kelas`
--
ALTER TABLE `kelas`
  ADD PRIMARY KEY (`idKelas`);

--
-- Indexes for table `pementor`
--
ALTER TABLE `pementor`
  ADD PRIMARY KEY (`idPementor`),
  ADD UNIQUE KEY `usernamePementor` (`usernamePementor`);

--
-- Indexes for table `pementor_kehadiran`
--
ALTER TABLE `pementor_kehadiran`
  ADD UNIQUE KEY `tanggalKehadiran` (`tanggalKehadiran`,`idPementor`);

--
-- Indexes for table `pementor_tanggungjawab`
--
ALTER TABLE `pementor_tanggungjawab`
  ADD UNIQUE KEY `idPementor` (`idPementor`,`idKelas`);

--
-- Indexes for table `siswa`
--
ALTER TABLE `siswa`
  ADD PRIMARY KEY (`idSiswa`);

--
-- Indexes for table `siswa_kehadiran`
--
ALTER TABLE `siswa_kehadiran`
  ADD UNIQUE KEY `tanggalKehadiran` (`tanggalKehadiran`,`idSiswa`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pementor`
--
ALTER TABLE `pementor`
  MODIFY `idPementor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `siswa`
--
ALTER TABLE `siswa`
  MODIFY `idSiswa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
