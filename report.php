<?php
require_once './functions/_const.php';
require_once './functions/model_kelas.php';
require_once './functions/model_kehadiran.php';
session_start();
if (!isset($_SESSION['login'])) {
    header('Location: login.php');
}

if(!isset($_GET['id'])){
    header('Location: index.php');
}

$idKelas = $_GET['id'];
$kelas = kelas_findById($idKelas)[0];
if(count($kelas) < 1){
    header('Location: index.php');
}

$dataKehadiran = kehadiran_getByClass($idKelas);
$dataPertemuan = kehadiran_getWeekByClass($idKelas);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Laporan</title>
    <h2 style="text-align: center">Laporan Kehadiran Mentoring Kelas <?= $kelas['namaKelas'] ?></h2>
    <table border="1" cellspacing="0" cellpadding="12px" style="overflow-x:scroll; white-space: nowrap;">
        <tr>
            <th>No.</th>
            <th>Nama</th>            
            <?php foreach($dataPertemuan as $id => $row) :?>
                <th>Pertemuan-<?= $id + 1 ?></th>
            <?php endforeach; ?>        
        </tr>
        <?php foreach($dataKehadiran as $id => $row) :?>
            <tr>
                <td><?= $id + 1 ?>.</td>
                <td><?= $row['namaSiswa'] ?></td>
                <?php if(count($row['daftarKehadiran']) < 1) :?>
                    <?php foreach($dataPertemuan as $id => $pertemuan) : ?>
                        <td>Tidak Hadir</td>
                    <?php endforeach?>
                <?php else : ?>
                    <?php foreach($dataPertemuan as $id => $pertemuan) : ?>                        
                        <?php if(isset($row['daftarKehadiran'][$id])): ?>
                        <?php $kehadiran = $row['daftarKehadiran'][$id]; ?>
                            <td><?= $kehadiran['tanggalKehadiran'] === $pertemuan['tanggalKehadiran'] ? 'Hadir' : 'Tidak Hadir' ?></td>                                                
                        <?php else: ?>
                            <td>Tidak Hadir</td>
                        <?php endif; ?>
                    <?php endforeach ?>
                <?php endif; ?>                
            </tr>
        <?php endforeach; ?>
    </table>    
</head>
<body>
    
</body>
</html>