<?php
require_once './functions/_const.php';
session_start();
if (!isset($_SESSION['login'])) {
    header('Location: login.php');
}
?>

<!DOCTYPE html>
<html lang="en">

<!-- HEADER -->

<head>
    <?php require_once './views/_header.php' ?>
    <title>SIMENTOR - Materi</title>
</head>

<!-- BODY -->

<body>
    <?php require_once './views/_navbar.php'; ?>
    <?php require_once './views/materi.php'; ?>
</body>

<!-- FOOTER -->
<?php require_once './views/_footer.php' ?>
<script>
    $(document).ready(function() {
        $('.collapsible').collapsible();
        $('.collapsible .expand').on('click', function(e) {
            var icon = $(this).hasClass('active') ? 'expand_more' : 'expand_less';
            $(this).find('.material-icons.expands').html(icon)
        })

        var floatingButton = $('.fixed-action-btn');
        var instances = M.FloatingActionButton.init(floatingButton, {
            hoverEnabled: false
        });

    });
</script>

</html>