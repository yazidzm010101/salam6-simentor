<?php

require_once '_database.php';

function siswa_findAll(){
    $queryString = "SELECT idSiswa, namaSiswa, jeniskSiswa, namaKelas FROM siswa, kelas WHERE siswa.idKelas = kelas.idKelas";
    return query($queryString);
}

function siswa_findByClass($classId){
    $queryString = "SELECT idSiswa, namaSiswa, namaKelas, jeniskSiswa FROM siswa, kelas WHERE siswa.idKelas = kelas.idKelas AND siswa.idKelas = '$classId' ORDER BY namaSiswa ASC";
    return query($queryString);
}

function siswa_findByClassAndGender($classId, $genderId){
    $queryString = "SELECT idSiswa, namaSiswa, namaKelas, jeniskSiswa FROM siswa, kelas WHERE siswa.idKelas = kelas.idKelas AND siswa.idKelas = '$classId' AND jeniskSiswa = '$genderId' ORDER BY namaSiswa ASC";
    return query($queryString);
}
