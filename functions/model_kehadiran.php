<?php

require_once '_database.php';
require_once 'model_siswa.php';

function kehadiran_input($data, $idPementor){    
    $tanggalKehadiran = $data['tanggalKehadiran'];
    //siswa
    $siswa = $data;
    unset($siswa['submit']);
    unset($siswa['tanggalKehadiran']);
    $siswa = array_keys($siswa);
        
    $queryString = "INSERT INTO pementor_kehadiran VALUES('$tanggalKehadiran', '$idPementor')";
    queryExec($queryString);
    foreach($siswa as $row){
        $queryString = "INSERT INTO siswa_kehadiran VALUES('$tanggalKehadiran', '$row')";
        queryExec($queryString);
    }
}

function kehadiran_getByClass($idKelas){
    $siswa = siswa_findByClass($idKelas);
    $results = [];

    foreach($siswa as $row){
        $idSiswa = $row['idSiswa'];

        $siswa = [];
        $siswa['idSiswa'] = $row['idSiswa'];
        $siswa['namaSiswa'] = $row['namaSiswa'];
        $siswa['namaKelas'] = $row['namaKelas'];
        $siswa['jeniskSiswa'] = $row['jeniskSiswa'];
        
        $queryString = "SELECT tanggalKehadiran FROM siswa, siswa_kehadiran WHERE siswa.idSiswa = siswa_kehadiran.idSiswa AND siswa_kehadiran.idSiswa = '$idSiswa' ORDER BY tanggalKehadiran ASC";
        $siswa['daftarKehadiran'] = query($queryString);

        array_push($results, $siswa);
    }

    return $results;
}

function kehadiran_getWeekByClass($idKelas){
    $queryString = "SELECT DISTINCT tanggalKehadiran FROM siswa, siswa_kehadiran WHERE siswa.idSiswa = siswa_kehadiran.idSiswa AND idKelas = '$idKelas' ORDER BY tanggalKehadiran ASC";
    return query($queryString);
}