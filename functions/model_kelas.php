<?php

require_once '_database.php';

function kelas_findAll(){
    $queryString = "SELECT * FROM kelas";
    return query($queryString);
}

function kelas_findById($id){
    $queryString = "SELECT * FROM kelas WHERE idKelas = '$id'";
    return query($queryString);
}

function kelas_findByCategory($cat){
    $queryString = "SELECT * FROM kelas WHERE idKelas LIKE '$cat%'";
    return query($queryString);
}

function kelas_sumByCategory($cat){
    $queryString = "SELECT
                    (SELECT COUNT(jeniskSiswa) FROM siswa WHERE jeniskSiswa = 'L' AND idKelas LIKE '$cat%') AS jumlahPria,
                    (SELECT COUNT(jeniskSiswa) FROM siswa WHERE jeniskSiswa = 'P' AND idKelas LIKE '$cat%') AS jumlahWanita";
    return query($queryString);
}

function kelas_sumByClass($classId){
    $queryString = "SELECT
                    (SELECT COUNT(jeniskSiswa) FROM siswa WHERE jeniskSiswa = 'L' AND idKelas LIKE '$classId') AS jumlahPria,
                    (SELECT COUNT(jeniskSiswa) FROM siswa WHERE jeniskSiswa = 'P' AND idKelas LIKE '$classId') AS jumlahWanita";
    return query($queryString);
}

function kelas_generateCategory(){
    $queryString = "SELECT DISTINCT LEFT(idKelas,2) as namaKelas FROM kelas";
    return query($queryString);
}
