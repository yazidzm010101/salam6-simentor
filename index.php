<?php 
    session_start();
    if(!isset($_SESSION['login'])){
        header('Location: login.php');
    }
    header('Location: daftar_kelas.php');
?>
<!DOCTYPE html>
<html lang="en">
<?php require_once './functions/_const.php' ?>
<head>
    <?php require_once './views/_header.php' ?>
    <link rel="stylesheet" href="<?= PUBLIC_URL ?>/public/css/login.min.css">
    <title>SIMENTOR - Dashboard</title>
</head>
<body>
<?php require_once './views/_navbar.php' ?>
</body>
<?php require_once './views/_footer.php' ?>
</html>