<?php
$data = ["Aqidah Islamiyah", "Al-Iman", "Rukun Islam", "Ma'rifullah", "Al-Qur'an", "Ukhuwah Islamiyah", "Nikmat Iman", "Pentingnya Akhlak Islami"];
?>
<div class="container">
    <div class="my-breadcrumbs">
        <a class="waves-effect waves-dark btn-flat btn-small" href="index.php">Dashboard</a>
        <i class="material-icons expand">chevron_right</i>
        <a class="waves-effect waves-dark btn-flat btn-small">Daftar Materi</a>
    </div>
    <div class="row">
        <div class="col s12 m6">                        
            <ul class="collection collapsible">                
                <li class="collection-item">
                    <strong>
                        <h6>Daftar Materi</h6>
                    </strong></li>                                
                <li class="active expand">
                    <div class="collapsible-header"><i class="material-icons expand">expand_less</i>Pembinaan Karakter Islami</div>
                    <div class="collapsible-body">
                        <?php foreach($data as $id => $row) : ?>
                            <div class="collection-item" style="height: calc(2.5rem + 20px)">
                                <img src="http://i3.ytimg.com/vi/lTigVB2Z5LU/hqdefault.jpg" style="float:left; height: 2.5rem ; margin-right:1rem;" alt="thumbnail">
                                <span style="float:left; line-height: 2.5rem; height: 2.5rem; max-width:60%; white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">#<?= $id + 1 ?> - <?= $row ?></span>
                                <a href="<?= PUBLIC_URL ?>/materi.php?id=1 ?>" class="secondary-content"><i class="material-icons green-text text-darken-2">chevron_right</i>
                                </a>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </li>
            </ul>            
        </div>
    </div>
</div>