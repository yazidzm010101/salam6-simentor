<?php
require_once './functions/model_kelas.php';
$daftarKategori = kelas_generateCategory();
?>

<div class="container">
    <div class="my-breadcrumbs">
        <a class="waves-effect waves-dark btn-flat btn-small" href="index.php">Dashboard</a>
        <i class="material-icons expand">chevron_right</i>
        <a class="waves-effect waves-dark btn-flat btn-small">Daftar Kelas</a>        
    </div>
    <div class="row">
        <div class="col s12 m6">
            <?php foreach($daftarKategori as $kategori) : ?>
            <?php
            $tingkat = $kategori['namaKelas'];                
            $daftarKelas = kelas_findByCategory($tingkat);
            $jumlahSiswa = kelas_sumByCategory($tingkat)[0];
            ?>
            <ul class="collection collapsible">                
                <li class="collection-item"><strong>
                        <h6>Keaktifan Kelas <?= $tingkat ?></h6>
                    </strong></li>
                <li class="collection-item">
                    <div style="margin-bottom: 0.5rem;">Jumlah siswa</div>
                    <span class="sum" style="margin: 0 1rem 0.5rem 0">
                        <span class="green darken-2" style="margin-right: 0.25rem"></span>
                        <span><?= $jumlahSiswa['jumlahPria'] ?> Ikhwan</span>
                    </span>
                    <span class="sum" style="margin: 0 1rem 0.5rem 0">
                        <span class="orange darken-1" style="margin-right: 0.25rem"></span>
                        <span><?= $jumlahSiswa['jumlahWanita'] ?> Akhwat</span>
                    </span>
                </li>
                <li class="collection-item">
                    <div style="margin-bottom: 0.5rem;">Rata-rata kehadiran</div>
                    <div style="margin-bottom: 0.5rem;">
                        <div style="display: inline-block; width: 84%">
                            <div class="progress grey lighten-2" style="height: 1rem; margin: 0.5rem 0">
                                <div class="determinate green darken-2" style="width: 80%"></div>                            
                            </div>
                        </div>
                        <div style="float: right; text-align: right; font-size:1rem; margin-top:4px"><strong>80%</strong></div>
                        <div style="display: inline-block; width: 84%">
                            <div class="progress grey lighten-2" style="height: 1rem; margin: 0.5rem 0">
                                <div class="determinate orange darken-1" style="width: 80%"></div>                            
                            </div>
                        </div>
                        <div style="float: right; text-align: right; font-size:1rem; margin-top:4px"><strong>80%</strong></div>
                    </div>                    
                </li>
                <li class="collection-item">
                    <img src="<?= PUBLIC_URL ?>/public/img/dummy_chart.png" alt="chart" style="width: 100%;">
                </li>
                <li class="active expand">
                    <div class="collapsible-header"><i class="material-icons expand">expand_less</i>Daftar Kelas</div>
                    <div class="collapsible-body">                            
                        <?php foreach($daftarKelas as $kelas) : ?>
                            <div class="collection-item"><?= $kelas['namaKelas'] ?><a href="<?= PUBLIC_URL ?>/kelas.php?id=<?= $kelas['idKelas'] ?>" class="secondary-content"><i class="material-icons green-text text-darken-2">chevron_right</i></a></div>
                        <?php endforeach; ?>
                    </div>
                </li>
            </ul>
            <?php endforeach; ?>
        </div>
    </div>
</div>