<?php
require_once './functions/model_kelas.php';
require_once './functions/model_siswa.php';
$kelas = null;
$daftarSiswa = null;

if(!isset($_GET['id'])){
    header('Location: index.php');
}else{
    $idKelas = $_GET['id'];
    $kelas = kelas_findById($idKelas);
    if (count($kelas) < 1) {
        header('Location: index.php');        
    }
    $kelas = $kelas[0];    
    $daftarSiswa = siswa_findByClass($idKelas);
    $jumlahSiswa = kelas_sumByClass($idKelas)[0];
}
?>

<div class="container">
    <div class="my-breadcrumbs">
        <a class="waves-effect waves-dark btn-flat btn-small" href="index.php">Dashboard</a>
        <i class="material-icons expand">chevron_right</i>
        <a class="waves-effect waves-dark btn-flat btn-small" href="daftar_kelas.php">Daftar Kelas</a>
        <i class="material-icons expand">chevron_right</i>
        <a class="waves-effect waves-dark btn-flat btn-small"><?= $kelas['namaKelas'] ?></a>
    </div>
    <div class="row">
        <div class="col s12 m6">
            <ul class="collection collapsible">
                <li class="collection-item"><strong>
                        <h6><?= $kelas['namaKelas'] ?></h6>
                    </strong></li>
                <li class="collection-item">
                    <div style="margin-bottom: 0.5rem;">Jumlah siswa</div>
                    <span class="sum" style="margin: 0 1rem 0.5rem 0">
                        <span class="green darken-2" style="margin-right: 0.25rem"></span>
                        <span><?= $jumlahSiswa['jumlahPria'] ?> Ikhwan</span>
                    </span>
                    <span class="sum" style="margin: 0 1rem 0.5rem 0">
                        <span class="orange darken-1" style="margin-right: 0.25rem"></span>
                        <span><?= $jumlahSiswa['jumlahWanita'] ?> Akhwat</span>
                    </span>
                </li>
                <li class="collection-item">
                    <div style="margin-bottom: 0.5rem;">Rata-rata kehadiran</div>
                    <div style="margin-bottom: 0.5rem;">
                        <div style="display: inline-block; width: 84%">
                            <div class="progress grey lighten-2" style="height: 1rem; margin: 0.5rem 0">
                                <div class="determinate green darken-2" style="width: 80%"></div>
                            </div>
                        </div>
                        <div style="float: right; text-align: right; font-size:1rem; margin-top:4px"><strong>80%</strong></div>
                        <div style="display: inline-block; width: 84%">
                            <div class="progress grey lighten-2" style="height: 1rem; margin: 0.5rem 0">
                                <div class="determinate orange darken-1" style="width: 80%"></div>
                            </div>
                        </div>
                        <div style="float: right; text-align: right; font-size:1rem; margin-top:4px"><strong>80%</strong></div>
                    </div>
                </li>
                <li class="collection-item">
                    <img src="<?= PUBLIC_URL ?>/public/img/dummy_chart.png" alt="chart" style="width: 100%;">
                </li>
                <li class="active expand">
                    <div class="collapsible-header"><i class="material-icons expand">expand_less</i>Daftar Siswa</div>
                    <div class="collapsible-body">
                        <?php foreach ($daftarSiswa as $row) : ?>
                            <div class="collection-item"><?= $row['namaSiswa'] ?></div>
                        <?php endforeach; ?>
                    </div>
                </li>
            </ul>
        </div>
    </div>

    <div class="fixed-action-btn">
        <a class="btn-floating btn-large waves-effect waves-light  orange darken-1">
            <i class="large material-icons">how_to_reg</i>
        </a>
        <ul>
            <li><a class="btn-floating red" href="<?= PUBLIC_URL ?>/report.php?id=<?= $idKelas ?>"><i class="material-icons">picture_as_pdf</i></a></li>            
            <li><a class="btn-floating blue" href="<?= PUBLIC_URL ?>/input_kehadiran.php?id=<?= $idKelas ?>"><i class="material-icons">playlist_add_check</i></a></li>
        </ul>
    </div>

</div>