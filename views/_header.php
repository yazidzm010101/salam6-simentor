<?php //head tag that used across pages ?>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Favicon -->
<link rel="icon" href="<?= PUBLIC_URL ?>/public/img/favicon.png" />
<!-- CSS -->
<link rel="stylesheet" href="<?= PUBLIC_URL ?>/public/css/googlefont.css">
<link rel="stylesheet" href="<?= PUBLIC_URL ?>/public/css/materialize.min.css">
<link rel="stylesheet" href="<?= PUBLIC_URL ?>/public/css/index.min.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">