<?php
if(isset($_POST['logout'])){
    $_SESSION = [];
    session_unset();
    session_destroy();
    header('Location: login.php');
}
?>
<nav class="white">
    <div class="nav-wrapper">
        <a href="#!" class="brand-logo" style="height: 56px; display:flex; align-items: center;"><img style="height: 45%;" src="<?= PUBLIC_URL ?>/public/img/SIMENTOR_landscape.png" alt=""></a>
        <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="grey-text text-darken-2 material-icons">menu</i></a>
        <ul class="right hide-on-med-and-down">
            <li><a href="sass.html">Sass</a></li>
            <li><a href="badges.html">Components</a></li>
            <li><a href="collapsible.html">Javascript</a></li>
            <li><a href="mobile.html">Mobile</a></li>
        </ul>
    </div>
</nav>

<ul class="sidenav" id="mobile-demo">
    <form action="" method="POST" style="text-align: center; padding: 2rem; position: relative;">    
        <img src="<?= PUBLIC_URL ?>/public/img/noprofile.png" alt="profile" style="height: 6rem">
        <button type="submit" name="logout" class="btn-flat waves-effect waves-dark" 
        style="position: absolute; right: 2rem; top: 6rem;"><i class="material-icons green-text text-darken-2">exit_to_app</i></button>
        <h6>MUHAMMAD RIZQI PANGESTU</h6>
    </form>
    <li><a href="<?= PUBLIC_URL ?>/index.php">Dashboard</a></li>
    <li><a href="<?= PUBLIC_URL ?>/daftar_kelas.php">Daftar Kelas</a></li>
    <li><a href="<?= PUBLIC_URL ?>/daftar_materi.php">Daftar Materi</a></li>
</ul>