<div class="container">
    <div class="my-breadcrumbs">
        <a class="waves-effect waves-dark btn-flat btn-small" href="index.php">Dashboard</a>
        <i class="material-icons expand">chevron_right</i>
        <a class="waves-effect waves-dark btn-flat btn-small" href="daftar_materi.php">Daftar Materi</a>
        <i class="material-icons expand">chevron_right</i>
        <a class="waves-effect waves-dark btn-flat btn-small">Aqidah Islamiyah</a>
    </div>
    <div class="row">
        <div class="col s12 m6">
            <ul class="collection collapsible">
                <li class="collection-item"><strong>
                        <h6>Aqidah Islamiyah</h6>
                    </strong></li>                
                <li class="collection-item" style="padding: 0;">
                    <iframe style="width: 100%; border: none;" src="https://www.youtube.com/embed/lTigVB2Z5LU"></iframe>                     
                </li>
                <div class="collection-item" style="line-height: 2rem">
                    <strong><h6>Pengertian Aqidah Islamiyah</h6></strong>
                    <p>Aqidah secara bahasa berasal dari kata ‘aqd yang berarti mempererat, mengokohkan, dan mengikat dengan kuat. Secara istilah aqidah adalah keyakinan yang kuat yang tidak dimasuki oleh keraguan.</p>
                    <p>Dengan demikian, aqidah Islam berarti keimanan yang kuat kepada Allah Ta’ala dengan  melaksanakan kewajiban berupa tauhid dan taat kepada-Nya, demikian juga beriman kepada malaikat-Nya, kitab-kitab-Nya, para rasul-Nya, hari akhir, dan beriman kepada qadar serta mengimani semua yang sudah shahih tentang prinsip-prinsip agama (ushuluddin), perkara-perkara yang ghaib, berita yang disebutkan dalam Alquran maupun sunah baik ‘ilmiyyah (sebagai pengetahuan yang harus diyakini) maupun ‘amaliyyah (pengetahuan yang harus diamalkan).</p>
                    <br>
                    <strong><h6>Pentingnya Aqidah Islamiyah atau Aqidah Ahlussunnah wal Jama'ah</h6></strong>
                    <p>Banyak orang yang mengaku dirinya Ahlussunnah wal Jama’ah, akan tetapi dalam perjalanannya ternyata banyak menyelisihi Aqidah Ahlussunnah wal Jama’ah. Hal ini tidak lain, karena pengenalan mereka tentang Ahlussunnah wal Jama’ah masih bersifat mujmal (garis besar) atau tidak terperinci.</p>
                    <p>Secara umum, memang mereka mengikuti sunnah Rasulullah shallallahu ‘alaihi wa sallam, karena tidak ada seorang muslim pun kecuali yang dijadikan acuan dalam hidupnya adalah Rasulullah shallallahu ‘alaihi wa sallam.</p>
                    <p>Akan tetapi sangat disayangkan, mereka tidak mengerti lebih rinci aqidah Ahlussunnah wal Jama’ah sehingga banyak praktik yang mereka lakukan ternyata bertentangan dengan aqidah Ahlussunnah wal Jama’ah.</p>
                </div>
            </ul>
        </div>
    </div>

    <div class="fixed-action-btn">
        <a class="btn-floating btn-large waves-effect waves-light  orange darken-1">
            <i class="large material-icons">how_to_reg</i>
        </a>
        <ul>
            <li><a class="btn-floating red"><i class="material-icons">picture_as_pdf</i></a></li>            
            <li><a class="btn-floating blue" href="<?= PUBLIC_URL ?>/input_kehadiran.php?id=<?= $idKelas ?>"><i class="material-icons">playlist_add_check</i></a></li>
        </ul>
    </div>

</div>