<?php
require_once './functions/model_kelas.php';
require_once './functions/model_siswa.php';
require_once './functions/model_kehadiran.php';

$kelas = null;
$daftarSiswa = null;
$idPementor = 1;
$namaPementor = 'MUHAMMAD RIZQI PANGESTU';
$jeniskPementor = 'L';

if(!isset($_GET['id'])){
    header('Location: index.php');
}else{
    $idKelas = $_GET['id'];
    $kelas = kelas_findById($idKelas);
    if (count($kelas) < 1) {
        header('Location: index.php');        
    }
    $kelas = $kelas[0];    
    $daftarSiswa = siswa_findByClassAndGender($kelas['idKelas'],$jeniskPementor);
}

if(isset($_POST['submit'])){
    kehadiran_input($_POST, $idPementor);
}

?>

<div class="container">
    <div class="my-breadcrumbs">
        <a class="waves-effect waves-dark btn-flat btn-small" href="<?= PUBLIC_URL ?>/index.php">Dashboard</a>
        <i class="material-icons expand">chevron_right</i>
        <a class="waves-effect waves-dark btn-flat btn-small" href="<?= PUBLIC_URL ?>/daftar_kelas.php">Daftar Kelas</a>
        <i class="material-icons expand">chevron_right</i>
        <a class="waves-effect waves-dark btn-flat btn-small" href="<?= PUBLIC_URL."/kelas.php?id=".$kelas['idKelas'] ?>"><?= $kelas['namaKelas'] ?></a>
        <i class="material-icons expand">chevron_right</i>
        <a class="waves-effect waves-dark btn-flat btn-small">Daftar Kehadiran</a>
    </div>
    <div class="row">
        <form action="input_kehadiran.php?id=<?= $idKelas ?>" method="POST" class="col s12 m6">
            <ul class="collection collapsible">
                <li class="collection-item"><strong>
                        <h6><?= $kelas['namaKelas'] ?></h6>
                    </strong></li>                
                <li class="collection-item">
                    <h6>Tanggal Kehadiran</h6>
                    <input type="text" class="datepicker" name="tanggalKehadiran" id="tanggalKehadiran" required>
                </li>
                <li class="collection-item">
                    <h6>Pementor</h6>
                    <p><strong><?= $namaPementor ?></strong></p>
                </li>
                <li class="active expand">
                    <div class="collapsible-header"><i class="material-icons expand">expand_less</i>Input Kehadiran</div>
                    <div class="collapsible-body" action="" method="POST">                        
                        <?php foreach ($daftarSiswa as $row) : ?>
                            <div class="collection-item">
                                <label>
                                    <input name="<?= $row['idSiswa'] ?>" type="checkbox" class="filled-in green darken-2" />
                                    <span class="grey-text text-darken-2"><?= $row['namaSiswa'] ?></span>
                                </label>                                
                            </div>
                        <?php endforeach; ?>
                        <div class="fixed-action-btn">
                            <button class="btn-floating btn-large waves-effect waves-light orange darken-1" name="submit"><i class="material-icons">done</i></button>
                        </div>
                    </div>
                </li>
            </ul>
        </form>
    </div>
    
        

</div>