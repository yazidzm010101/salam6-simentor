<?php
session_start();
if(isset($_SESSION['login'])){
    header('Location: index.php');
}
if(isset($_POST['submit'])){
    if($_POST['username'] === 'testing' && $_POST['password'] === 'password'){
        $_SESSION['login'] = true;
        header('Location: index.php');
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<?php require_once './functions/_const.php'?>
<head>
    <?php require_once './views/_header.php' ?>
    <link rel="stylesheet" href="<?= PUBLIC_URL ?>/public/css/login.min.css">
    <title>SIMENTOR - Login</title>
</head>
<body>
    <div class="container grey-text text-darken-3">
        <section id="login-form">
            <div class="row">
                <div class="col s12">
                    <img class="login-org" src="<?= PUBLIC_URL ?>/public/img/salam6_logo.png" alt="Salam 6" height="20">
                    <img class="login-prd-img" src="<?= PUBLIC_URL ?>/public/img/simentor_logo_main.png" alt="SIMENTOR" height="80">
                    <h6 class="login-prd-txt">Sistem Informasi Mentoring</h6>
                </div>
                <form action="" method="POST" class="form col s12" autocomplete="off">
                    <div class="row">
                        <div class="input-field col s12">
                            <input id="username" name="username" type="text" class="validate">
                            <label for="username">Nama Pengguna</label>
                            <span class="helper-text" data-error="Nama pengguna belum diisi!"></span>
                        </div>
                        <div class="input-field col s12">
                            <input id="password" name="password" type="password" class="validate">
                            <label for="password">Kata Sandi</label>
                            <span class="helper-text" data-error="Kata sandi belum diisi!"></span>
                        </div>
                        <div class="input-field col s12">
                            <a id="a-forgot" class="waves-effect btn-flat orange-text text-darken-1 left" style="height: 100%;">Lupa kata sandi?</a>
                            <button class="waves-effect waves-light btn orange darken-1 right" type="submit" name="submit">Masuk</button>
                        </div>
                    </div>
                </form>
            </div>
        </section>
    </div>
</body>
<?php require_once './views/_footer.php'?>
</html>